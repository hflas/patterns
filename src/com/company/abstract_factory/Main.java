package com.company.abstract_factory;

import com.company.abstract_factory.factories.AbstractFactory;
import com.company.abstract_factory.factories.ElfFactory;
import com.company.abstract_factory.units.Elf;
import com.company.abstract_factory.units.Unit;

public class Main {
    public static void main(String[] args) {
        AbstractFactory abstractFactory =
                new ElfFactory();
        Elf elfWizard = abstractFactory.getWizard();
        System.out.println(elfWizard.getName());

    }
}
