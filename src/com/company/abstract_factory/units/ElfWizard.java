package com.company.abstract_factory.units;

public class ElfWizard implements Elf {
    @Override
    public String getName() {
        return "ElfWizard";
    }
}
