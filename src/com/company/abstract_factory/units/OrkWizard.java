package com.company.abstract_factory.units;

public class OrkWizard implements Ork {
    @Override
    public String getName() {
        return "OrkWizard";
    }
}
