package com.company.abstract_factory.units;

public interface Unit {
    String getName();
}
