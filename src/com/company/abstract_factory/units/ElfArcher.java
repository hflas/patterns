package com.company.abstract_factory.units;

public class ElfArcher implements Elf {
    @Override
    public String getName() {
        return "ElfArcher";
    }
}
