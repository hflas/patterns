package com.company.abstract_factory.factories;

import com.company.abstract_factory.units.Unit;

public interface AbstractFactory {
    Unit getWizard();
    Unit getArcher();
}
