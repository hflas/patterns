package com.company.abstract_factory.factories;

import com.company.abstract_factory.units.*;

public class ElfFactory implements AbstractFactory {

    @Override
    public Unit getWizard() {
        return new ElfWizard();
    }

    @Override
    public Unit getArcher() {
        return new ElfArcher();
    }
}
