package com.company.abstract_factory.factories;

import com.company.abstract_factory.units.*;

public class OrkFactory implements AbstractFactory {

    @Override
    public Unit getWizard() {
        return new OrkWizard();
    }

    @Override
    public Unit getArcher() {
        return new OrkArcher();
    }
}
