package com.company.factory_method;

public class Main {
    public static void main(String[] args) {
        ColorFactory whiteColorFactory = new WhiteColorFactory();
        createColor(whiteColorFactory);

        ColorFactory blackColor = new BlackColorFactory();
        createColor(blackColor);
    }

    public static void createColor(ColorFactory colorFactory){
        System.out.println(colorFactory.getColor());
    }
}
