package com.company.factory_method;

public class BlackColorFactory extends ColorFactory{
    @Override
    protected Color createColor() {
        return new BlackColor();
    }
}
