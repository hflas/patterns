package com.company.factory_method;

public class WhiteColorFactory extends ColorFactory{
    @Override
    protected Color createColor() {
        return new WhiteColor();
    }
}
