package com.company.factory_method;

public class WhiteColor implements Color {
    @Override
    public String getName() {
        return "WhiteColor";
    }
}
