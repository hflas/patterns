package com.company.factory_method;

public class BlackColor implements Color {
    @Override
    public String getName() {
        return "BlackColor";
    }
}
