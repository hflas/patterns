package com.company.factory_method;

public abstract class ColorFactory {
    private Color color;

    protected abstract Color createColor();

    public String getColor(){
        color = createColor();
        return color.getName();
    }
}
