package com.company.factory_method;

public interface Color {
    String getName();
}
